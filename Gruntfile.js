/*
 * Generated on 2015-03-10
 * generator-assemble v0.5.0
 * https://github.com/assemble/generator-assemble
 *
 * Copyright (c) 2015 Hariadi Hinta
 * Licensed under the MIT license.
 *
 *
 */

'use strict';

module.exports = function (grunt) {

    require('time-grunt')(grunt);
    require('load-grunt-tasks')(grunt);

    // Project configuration.
    grunt.initConfig({

        config: {
            src: 'src',
            dist: 'dist'
        },

        watch: {
            assemble: {
                files: ['<%= config.src %>/{content,data,templates}/**/*.{md,hbs,yml,json}'],
                tasks: ['assemble', 'handlebars', 'neuter:application']
            },
            sass: {
                files: ['<%= config.src %>/assets/styles/**/*.scss'],
                tasks: ['sass:dist', 'postcss']
            },
            images: {
                files: ['<%= config.src %>/assets/images/**/*.{jpg,jpeg,png,gif}'],
                tasks: ['copy:images']
            },
            scripts: {
                files: ['<%= config.src %>/assets/scripts/*.js', '<%= config.src %>/assets/scripts/{,*/}*/{,*/}*.js', '<%= config.dist %>/assets/scripts/*.js'],
                tasks: ['concat', 'copy:js', 'neuter:application'],
                options: {
                    spawn: false,
                }
            },
            json: {
                files: ['<%= config.src %>/data/mock-json/*.json'],
                tasks: ['copy:json']
            },
            gruntfile: {
                files: ['Gruntfile.js'],
                tasks: ['build']
            }
        },

        browserSync: {
            serve: {
                bsFiles: { src: ['<%= config.dist %>/**/*.*'] },
                options: {
                    watchTask: true,
                    server: '<%= config.dist %>',
                    // tunnel: true,
                    open: 'external',
                    notify: false,
                    ghostMode: {
                        clicks: true,
                        forms: true,
                        scroll: true
                    }
                }
            }
        },

        assemble: {
            options: {
                flatten: true,
                assets: '<%= config.dist %>/assets',
                layoutdir: '<%= config.src %>/templates/layouts/',
                data: '<%= config.src %>/data/**/*.{json,yml}',
                helpers: ['<%= config.src %>/assets/scripts/template_helpers.js'],
                partials: '<%= config.src %>/templates/partials/**/*.hbs'
            },
            pages_site: {
                options: {
                    layout: 'default.hbs',
                },
                files: [{
                    expand: true,
                    cwd: '<%= config.src %>/templates/pages--site',
                    src: ['**/*.hbs'],
                    dest: '<%= config.dist %>/'
                }]
            },
            pages_styleguide: {
                options: {
                    layout: 'styleguide-layout.hbs',
                },
                files: [{
                    expand: true,
                    cwd: '<%= config.src %>/templates/pages--styleguide',
                    src: ['**/*.hbs'],
                    dest: '<%= config.dist %>/styleguide/'
                }]
            }
        },

        copy: {

            bootstrap: {
                expand: true,
                cwd: 'bower_components/bootstrap-sass/assets/javascripts/',
                src: 'bootstrap.min.js',
                dest: '<%= config.dist %>/assets/scripts/lib/',
                flatten: true,
                filter: 'isFile'
            },
            customfonts: {
                expand: true,
                cwd: '<%= config.src %>/assets/fonts/',
                src: '**',
                dest: '<%= config.dist %>/assets/fonts/'
            },
            js: {
                expand: true,
                cwd: '<%= config.src %>/assets/scripts/',
                src: '*.js',
                dest: '<%= config.dist %>/assets/scripts/',
                flatten: true,
                filter: 'isFile'
            },
            images: {
                expand: true,
                cwd: '<%= config.src %>/assets/images/',
                src: '**',
                dest: '<%= config.dist %>/assets/images/',
                filter: 'isFile'
            },
            jquery: {
                expand: true,
                cwd: 'bower_components/jquery/dist/',
                src: 'jquery.min.js',
                dest: '<%= config.dist %>/assets/scripts/lib/',
                flatten: true,
                filter: 'isFile'
            },
            jquery_mockjax: {
                expand: true,
                cwd: 'bower_components/jquery-mockjax/dist/',
                src: 'jquery.mockjax.min.js',
                dest: '<%= config.dist %>/assets/scripts/lib/',
                flatten: true,
                filter: 'isFile'
            },
            validate: {
                expand: true,
                cwd: 'bower_components/bootstrap-validator/dist/',
                src: 'validator.min.js',
                dest: '<%= config.dist %>/assets/scripts/lib/',
                flatten: true,
                filter: 'isFile'
            },
            handlebars: {
                expand: true,
                cwd: 'bower_components/handlebars/',
                src: 'handlebars.min.js',
                dest: '<%= config.dist %>/assets/scripts/lib/',
                flatten: true,
                filter: 'isFile'
            },
            clipboard: {
                expand: true,
                cwd: 'bower_components/clipboard/dist/',
                src: 'clipboard.min.js',
                dest: '<%= config.dist %>/assets/scripts/lib/',
                flatten: true,
                filter: 'isFile'
            },
            prism: {
                expand: true,
                cwd: 'bower_components/prism/',
                src: 'prism.js',
                dest: '<%= config.dist %>/assets/scripts/lib/'
            },
            json: {
                expand: true,
                cwd: '<%= config.src %>/data/mock-json/',
                src: '*.json',
                dest: '<%= config.dist %>/assets/json'
            },
            slickCarousel: {
                expand: true,
                cwd: 'bower_components/slick-carousel/slick/',
                src: 'slick.min.js',
                dest: '<%= config.dist %>/assets/scripts/lib/'
            },
            bootstrapSrc: {
                expand: true,
                cwd: 'bower_components/bootstrap-sass/assets/stylesheets/',
                src: [
                  'bootstrap/**/*.scss',
                  '_bootstrap.scss'
                ],
                dest: '<%= config.src %>/assets/styles/Foundation/Themes/code/stylesheets/'
            },
            bootstrapAsset: {
                expand: true,
                cwd: 'bower_components/bootstrap-sass/assets/fonts/bootstrap',
                src: '**',
                dest: '<%= config.dist %>/assets/fonts/bootstrap'
            },
            moment: {
                expand: true,
                cwd: 'bower_components/moment/min/',
                src: 'moment.min.js',
                dest: '<%= config.dist %>/assets/scripts/lib/'
            },
            dateTimePickerJS: {
                expand: true,
                cwd: 'bower_components/eonasdan-bootstrap-datetimepicker/build/js/',
                src: 'bootstrap-datetimepicker.min.js',
                dest: '<%= config.dist %>/assets/scripts/lib/'
            }
        },

        sass: { // Task
            dist: { // Target
                options: {
                    style: 'compressed'
                },
                files: { // Dictionary of files
                    "<%= config.dist %>/assets/styles/default.css": "<%= config.src %>/assets/styles/Foundation/Themes/code/stylesheets/default.scss",
                    "<%= config.dist %>/assets/styles/main.css": "<%= config.src %>/assets/styles/Project/Common/code/stylesheets/main.scss",
                    "<%= config.dist %>/assets/styles/aclara.css": "<%= config.src %>/assets/styles/Project/Common/code/stylesheets/aclara.scss"
                }
            }
        },

        postcss: {
            options: {
                map: true,
                processors: [
                  require('autoprefixer')({ browsers: ['last 3 versions'] })
                ]
            },
            dist: {
                src: '<%= config.dist %>/assets/styles/**/*.css'
            }
        },

        neuter: {
            options: {
                basePath: '<%= config.src %>/assets/scripts/require/'
            },
            application: {
                src: '<%= config.src %>/assets/scripts/main.js',
                dest: '<%= config.dist %>/assets/scripts/main.js'
            }
        },

        concat: {
            dist: {
                files: {
                    '<%= config.dist %>/assets/scripts/lib.js': [
                      '<%= config.dist %>/assets/scripts/lib/jquery.min.js',
                      '<%= config.dist %>/assets/scripts/lib/validator.min.js',
                      '<%= config.dist %>/assets/scripts/lib/jquery.mockjax.min.js',
                      '<%= config.dist %>/assets/scripts/lib/handlebars.min.js',
                      '<%= config.dist %>/assets/scripts/lib/clipboard.min.js',
                      '<%= config.dist %>/assets/scripts/lib/prism.js',
                      '<%= config.dist %>/assets/scripts/lib/slick.min.js',
                      '<%= config.dist %>/assets/scripts/lib/bootstrap.min.js',
                      '<%= config.dist %>/assets/scripts/lib/moment.min.js',
                      '<%= config.dist %>/assets/scripts/lib/bootstrap-datetimepicker.min.js'
                    ]
                }
            }
        },

        handlebars: {
            options: {
                namespace: 'Ameren.Templates',
                processName: function (filePath) {
                    return filePath.replace(/^src\/templates\/partials\/SPA-Templates\//, '').replace(/\.hbs$/, '');
                },
                partialsUseNamespace: true
            },
            all: {
                files: {
                    "<%= config.src %>/assets/scripts/templates.js": ["<%= config.src %>/templates/partials/SPA-Templates/*.hbs"]
                }
            }
        },

        clean: ['<%= config.dist %>/**/*.{html,xml}']

    });

    grunt.loadNpmTasks('assemble');

    grunt.registerTask('build', [
      'clean',
      'assemble',
      'handlebars',
      'sass',
      'postcss',
      'copy',
      'concat',
      'neuter'
    ]);


    grunt.registerTask('default', ['build']);
    grunt.registerTask('serve', ['build', 'browserSync', 'watch']);

};
