// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  var geolocation = {
    lat: 38.625058,
    lng: -90.209557
  };
  var circle = new google.maps.Circle({
    center: geolocation,
    radius: 100
  });
  autocomplete.setBounds(circle.getBounds());
}

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  if (document.getElementById('autocomplete')) {
    autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
      {types: ['geocode']});
  }

  //autocomplete.addListener('place_changed', checkAddress);
}

function checkAddress() {
  $('#tab-step-2 a').trigger('click');
}

$(document).ready(function(){

  // Use email address for username checkbox
  $('#use_email_for_username').change(function(e){
    this.checked
      ? $('#username_field').val('jsmith@gmail.com')
      : $('#username_field').val('');
  });

  // Show password checkbox
  $('#show_password').change(function(e){
    this.checked
      ? $('#password_field').prop('type', 'text')
      : $('#password_field').prop('type', 'password');
  });
  //password_field

  initAutocomplete();
});
