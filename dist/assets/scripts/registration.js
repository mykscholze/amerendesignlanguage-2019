$(document).ready(function () {
    $.validator.setDefaults({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
        }
    });
    $("#verify-mobile, #register-mobile, #verify-email, #register-email").each(function () {
        $(this).validate({
            rules: {
                mobile: { required: true, minlength: { param: 14 } },
                verify_code: {
                    required: true,
                    minlength: {
                        param: 8,
                        depends: function (element) {
                            return $("#verify_code").val() != '123456';
                        }
                    }
                },
                email: {
                    required: true,
                    email: true
                },
                verify_email: {
                    required: true,
                    minlength: {
                        param: 8,
                        depends: function (element) {
                            return $("#verify_email").val() != '654321';
                        }
                    }
                },
            },
            messages: {
                mobile: {
                    required: "Please enter your mobile phone number",
                    minlength: "Please enter a valid mobile phone number"
                },
                email: "Please enter your email address",
                verify_code: {
                    required: "Code is a required field (code = 123456)",
                    minlength: "Please enter the valid code (code = 123456)"
                },
                verify_email: {
                    required: "Code is a required field (code = 654321)",
                    minlength: "Please enter the valid code (code = 654321)"
                }
            }
        });
    });
    // Show mobile verify code input
    $('#register-mobile').submit(function (e) {
        if ($(this).valid()) {
            e.preventDefault();
            $('#verify_code').closest('.form-group').hide().stop().slideDown('fast');
            $('#verify_code').val("");
            $('#send_code').html('Resend Code');
        }
    });

    $('#register-email').submit(function (e) {
        if ($(this).valid()) {
            e.preventDefault();
            $('#verify_email').closest('.form-group').hide().stop().slideDown('fast');
            $('#verify_email').val("");
            $('#send_email').html('Resend Email');
        }
    });
    $('form.verify').submit(function (e) {
        if (!$(this).valid()) {
            e.preventDefault();
        }
    });
    $("[name='mobile']").mask("(999) 999-9999");
    $('.verify_code').mask('999999');
    $('#accountNumber').mask('99999-99999');
    $('#ssn').mask('999-99-9999');
  //password_field
});
