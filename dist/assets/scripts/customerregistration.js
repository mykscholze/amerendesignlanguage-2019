﻿$(document).ready(function () {
    $.validator.setDefaults({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
        }
    });
    $("#customer-info").each(function () {
        $(this).validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                first_name: { required: true },
                last_name: { required: true },
                username_field: { required: true },
                password_field: { required: true },
                security_answer: { required: true },
                terms_privacy: { required: true },
                security_question: {
                    required: true,
                    depends: function (element) {
                        if ('0' == $("#security_question").val()) {
                            //Set predefined value to blank.
                            $('#security_question').val('');
                        }
                    }
                },
            },
            messages: {
                email: "Please enter your email address",
                first_name: "Please enter your first name",
                last_name: "Please enter your first name",
                username_field: "Please enter user name",
                password_field: "Please enter password",
                security_answer: "Please enter security answer",
                terms_privacy: "Please select privacy checkbox",
                security_question: "Please select security question",
            }
        });
    });
    $('form.verifycustomer').submit(function (e) {
        if (!$(this).valid()) {
            e.preventDefault();
        }
    });
    // Use email address for username checkbox
    $('#use_email_for_username').change(function (e) {
        this.checked
            ? $('#username_field').val($('#email').val())
            : $('#username_field').val('');
        if (this.checked) {
            $("#userid_requirements").addClass("hide");
        }

    });

    // Show password checkbox
    $('#show_password').change(function (e) {
        this.checked
            ? $('#password_field').prop('type', 'text')
            : $('#password_field').prop('type', 'password');
    });
    $('#email').val('jsmith@gmail.com');
  
    $("#username_field").on('focus', function () {
        // clear and hide main error msg, then show requirements checklist 
        $("#userid_requirements").removeClass("hide");
    })
        .on('blur', $.proxy(function () {
            $("#userid_requirements").addClass("hide");
        }, this))
        .on('keyup', $.proxy(function () {
            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");

            if ($("#username_field").val().length >= 6) {
                $("#6char").removeClass("glyphicon-remove");
                $("#6char").addClass("glyphicon-ok");
            } else {
                $("#6char").removeClass("glyphicon-ok");
                $("#6char").addClass("glyphicon-remove");
            }

            if (ucase.test($("#username_field").val())) {
                $("#ucase").removeClass("glyphicon-remove");
                $("#ucase").addClass("glyphicon-ok");

            } else {
                $("#ucase").removeClass("glyphicon-ok");
                $("#ucase").addClass("glyphicon-remove");
            }

            if (lcase.test($("#username_field").val())) {
                $("#lcase").removeClass("glyphicon-remove");
                $("#lcase").addClass("glyphicon-ok");
            } else {
                $("#lcase").removeClass("glyphicon-ok");
                $("#lcase").addClass("glyphicon-remove");
            }

            if (num.test($("#username_field").val())) {
                $("#num").removeClass("glyphicon-remove");
                $("#num").addClass("glyphicon-ok");
            } else {
                $("#num").removeClass("glyphicon-ok");
                $("#num").addClass("glyphicon-remove");
            }
        }, this));
    $("#password_field").on('focus', function () {
        // clear and hide main error msg, then show requirements checklist 
        $("#password_requirements").removeClass("hide");
    })
        .on('blur', $.proxy(function () {
            $("#password_requirements").addClass("hide");
        }, this))
        .on('keyup', $.proxy(function () {
            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");
            $("#password_requirements").removeClass("hide");
            if ($("#password_field").val().length >= 8) {
                $("#8char").removeClass("glyphicon-remove");
                $("#8char").addClass("glyphicon-ok");
            } else {
                $("#8char").removeClass("glyphicon-ok");
                $("#8char").addClass("glyphicon-remove");
            }

            if (ucase.test($("#password_field").val())) {
                $("#password_ucase").removeClass("glyphicon-remove");
                $("#password_ucase").addClass("glyphicon-ok");

            } else {
                $("#password_ucase").removeClass("glyphicon-ok");
                $("#password_ucase").addClass("glyphicon-remove");
            }

            if (lcase.test($("#password_field").val())) {
                $("#password_lcase").removeClass("glyphicon-remove");
                $("#password_lcase").addClass("glyphicon-ok");
            } else {
                $("#password_lcase").removeClass("glyphicon-ok");
                $("#password_lcase").addClass("glyphicon-remove");
            }

            if (num.test($("#password_field").val())) {
                $("#password_num").removeClass("glyphicon-remove");
                $("#password_num").addClass("glyphicon-ok");
            } else {
                $("#password_num").removeClass("glyphicon-ok");
                $("#password_num").addClass("glyphicon-remove");
            }
        }, this));
});
