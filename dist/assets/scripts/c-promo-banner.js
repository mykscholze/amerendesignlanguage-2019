var PromoBanner = (function() {

    var options = {
        dots: true,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        accessibility: true,
        prevArrow: '<button type="button" class="slick-prev" aria-label="Previous"><svg viewBox="0 0 24 24"><polyline points="10.5 4.5 17.66 11.99 10.5 19.5"/></svg></button>',
        nextArrow: '<button type="button" class="slick-next" aria-label="Next"><svg viewBox="0 0 24 24"><polyline points="10.5 4.5 17.66 11.99 10.5 19.5"/></svg></button>'
    };
    
    var init = function() {

        var $promoBanners = $('.c-promo-banner');

        $promoBanners.slick(options);
    }
  
    return {
        init: init
    };
  })();
  
  $(document).ready(function(){
    PromoBanner.init();
  });
  