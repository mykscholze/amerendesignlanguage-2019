$(document).ready(function(){

  // Enable and focus into other payment amount field
  $('[name="payment-amount"]').change(function(e){
    this.value == 'other'
      ? $('#payment-amount-text').prop('disabled', false).focus()
      : $('#payment-amount-text').prop('disabled', true);
  });

  // Show account nickname field
  $('[name="save-account-information"]').change(function(e){
    this.checked
      ? $('#account-nickname').closest('.form-group').slideDown('fast')
      : $('#account-nickname').closest('.form-group').slideUp('fast');
  });

});
