# Gitflow - Repository Checkout and Workflow

## Table of Contents

1. [Read the Gitflow Documentation](#1-read-the-gitflow-documentation)
2. [Install Git](#2-install-git)
  * [Best Practices](#best-practices)
3. [Code Checkout](#3-code-checkout)
4. [Working with Branches](#4-working-with-branches)
  * [Which branch does what?](#which-branch-does-what)
  * [Creating feature branches](#creating-feature-branches)
5. [Commiting](#5-commiting)
  * [Stage Changes](#stage-changes)
  * [Commit](#commit)

### 1. Read the Gitflow Documentation.

There is a wonderful explanation of different workflow approaches for using Git at Atlassian’s website.  If you’re new to Git, or even if you’ve been using it for a while, reading this outline will help you to understand how we will be using Git on this project.  It first goes over the Centralized Workflow, then the Feature Branch Workflow, and finally getting to the Gitflow Workflow.

### 2. Install Git

Install Sourcetree.  There are Windows and Mac versions of it, and I’ve found it to be the most robust yet easiest to use Git client out there.  Using Git in the Command Prompt or Terminal is also an option, but the Sourcetree GUI makes working with Git much simpler.

#### Best Practices
1. Make small commits, and very often.  Don’t wait until the end of the day or completion of a feature to commit.
2. Commit messages should explain the changes. These should be fairly specific, since your commit should be small.
3. Only push changes that you know will not break the build.
4. Pull from the repository as often as needed (before you start working, and before you push changes at the minimum).

### 3. Code Checkout

To clone the repository within Sourcetree, go to File > New / Clone and select Clone from URL.  The Source URL for our Repository is [https://user.name@gitlab.perficientxd.com/Ameren/Mobile-First-Redesign.git](https://user.name@gitlab.perficientxd.com/Ameren/Mobile-First-Redesign.git).  Note that "user.name" in that URL should be replaced with your Gitlab username.  It should be "firstname.lastname" format.  When Sourcetree validates the URL, it should ask you to enter your username and password.  Provide those (Gitlab credentials), and click OK.

Change your destination path and project name as you feel necessary, then click Clone.

After cloning is complete, you should see another popup asking for your full name and email address.  This is what will be displayed next to each of your commit messages.

#### NPM Install and Grunt Serve

Now that the code is on your computer, there's only one more step to get up and running.  Open a Command Prompt or Terminal window and `cd` into the directory that has the cloned repository.  

Enter the command `npm install` as this will make Node install all the required dependencies for the project.

With that done, and any time you wish to start your local Node serve and do your front-end development work, type the command `grunt serve` inside the repository directory.

---
### 4. Working with Branches

Git was designed to make branching and merging a lot easier than other version control systems. This is a necessity because Git is distributed. If you are new to Git, I recommend trying some of the following interactive tutorials.

* [Try Git](https://try.github.io/levels/1/challenges/1)
* [Learning Git Branching](http://pcottle.github.io/learnGitBranching/)

Press the “Git Flow” button at the top and accept all the default options.  This will now give you a local master branch to track the remote master branch, as well as a local develop branch to track the remote develop branch.

*Edit: For Mac users, the latest versions of SourceTree have removed the Git Flow button from the toolbar, but it can still be used by pressing Cmd+Opt+F.*

#### Which branch does what?

* **master** – The master branch holds all of our production-ready code, so at any given point, an AEM developer should be able to pull the code from master and run with it.  There should be no development done on or commits made directly to the master branch. At the end of each sprint, we will take the code on our finished develop branch and merge it down to master, including the tag for the current sprint.  That is the only time master will be updated.

* **develop** – The develop branch serves as an integration branch for all of our features.  Like the master branch, there should not be any direct commits made to the develop branch, but instead throughout the sprint each completed feature will be merged into the develop branch.

* **feature/feature-name** – Feature branches are where your actual development will happen.  Breaking out new features into their own branch allows us each to work on multiple things at once, and switch back and forth without worrying about one set of code affecting the other.

* **hotfix/hotfix-name** - After a sprint has completed and the master branch is updated, if there are bugs identified as showstoppers that need to be completed right away, a hotfix branch can be created.  When finished, a hotfix branch is automatically merged into develop and into master, so that the develop branch can be kept up to date, and so that the master branch can be pulled by the AEM developers once more.

#### Creating feature branches

Before creating a new feature, you will want to make sure your local develop branch is current and has the latest code.  Double click your develop branch in the sidebar to checkout that branch, then select “Pull” from the menu, and ensure that you are pulling from origin / develop.  Press OK, and your develop branch is now current.

Going back to the Git Flow dropdown, pressing it now will offer you a few actions.  To create a new feature branch select “Start a New Feature”.  Name your branch something that is all lowercase-with-dashes, and ensure that you are starting at the latest development branch.

---
### 5. Committing

#### Stage Changes

Go to your Working Copy view inside Sourcetree and select which files you’d like to commit.  Remember, commit small chunks, so even if you have a lot of files with changes, you can pick and choose which to add to your commits individually.

#### Commit

When you’ve staged the files you wish to commit, add your commit message to describe what the commit contains and press the Commit button.

---
### 6. Keeping Branches Current

Just as you did when you started your feature, before you finish it you want to make sure you have all the latest code from develop in your feature.  To do this, checkout your feature branch by double clicking it in the branch tree, then select Pull from the menu.  Select “develop” as the remote branch to pull, and you should be pulling into your feature branch.  Before selecting OK, check “Rebase instead of merge” in the options.  Now your feature branch will fast-forward to be on top of the develop branch if there were other changes ahead of your feature.

---
### 7. Submitting a Feature Branch for Review

Once your feature is finished, you’ll need to submit it for review.  This can only be done from the Gitlab website.  First you will push your local feature branch to a new remote branch (if there is not currently a remote for your feature).  With your feature branch as a remote, log into the Gitlab site and select “Merge Requests” from the project page.  Then select “New Merge Request”, and specify your feature branch as the source and the develop branch as the target.  Click “Compare branches”, and then fill out a description of what your feature branch includes (you can leave the title as the name of your branch).  Assign the merge request to Zach Handing and submit the request. You should follow this up with an email to Zach letting him know there a branch for review.  
