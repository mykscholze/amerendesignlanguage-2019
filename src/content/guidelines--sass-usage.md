## Sass Usage

Any CSS written will be put into `.scss` files inside the `/src/assets/sass/` directory.  All of these stylesheets get compiled together into one `main.css` file when Grunt does a build.  Each new stylesheet created should be prefixed with an underscore to assign them as partials and open them up to our variables and mixins.  In order to see any new styles, after the stylesheet has been created it should be added to the import list in `main.scss`.

Following our principles of Atomic Design, stylesheets should be broken up into atoms, molecules and organisms.

* Atom stylesheets are for general styles on individual elements, or for helper classes.
* Molecule stylesheets are for the very basic structural styling of reusable molecules.  These should be limited to what makes that molecule that molecule (e.g. search form has 30px height and 100% width, search button absolutely positioned to the right).  Alternative styling should be abstracted out from the base styles.
* Organism stylesheets are for component-specific styles.  Any style you would apply to the organism OR styles for the molecules inside that only apply to that specific organism.  For example, the search form contained in the header might have a margin-top of 2rem, but that's not to say every search form placed on the site should have a margin-top of 2rem.  That margin-top rule should be kept localized to the header organism stylesheet.
