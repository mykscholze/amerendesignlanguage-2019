# New Developer Onboarding

## Table of Contents
1. [Introduction to the Front-End Code](#introduction-to-the-front-end-code)
2. [Git Repository Access](#git-repository-access)
3. [Install Ruby (Windows Only)](#install-ruby-windows-only)
4. [Install Sass](#install-sass)
5. [Install Node](#install-node)
6. [Install Grunt](#install-grunt)
7. [Install SourceTree](#install-sourcetree)

---

### 1. Introduction to the Front-End Code
The front-end development stack we will be using is a heavily customized version of the static-site generator [Assemble](http://assemble.io), which boils down to a combination of [Grunt](http://gruntjs.com), [Handlebars](http://handlebarsjs.com), [Sass](http://sass-lang.com), [Bootstrap](http://getbootstrap.com/css/) and [Atomic Design](http://bradfrost.com/blog/post/atomic-web-design/).  Grunt is for compiling our Handlebars templates into HTML pages, as well as compiling our Sass into CSS.  Handlebars allows us to write modular HTML components.  Sass does the same for our CSS.  We’re using a custom build of Bootstrap that only includes the grid, responsive utility classes, and a handful of global CSS styles; most of the components have been stripped out. Atomic design is the principle we will be following for writing our HTML components; breaking up our components into the smallest possible chunks for reusability and consistency.

## THINGS TO DO BEFORE YOU'RE READY TO START WORKING

---


### 2. Git Repository Access

Email [Zach Handing](mailto:zach.handing@perficient.com) and request access to the Mobile First Redesign git repository.

Be sure to include the email address you would like your account set up under (if different from your sending address), and what level of access you will need (read, or read and write).


### 3. Install Ruby (Windows Only)
[http://rubyinstaller.org/downloads](http://rubyinstaller.org/downloads)

Ruby comes packaged by default on Mac machines, so if you're on Windows you'll need to install it.  We will be using Ruby to install Sass in the next step.

1. Download and install the latest stable Ruby build.
2. Open a command prompt and run `ruby -v` to ensure install was successful.
3. Might need to add Ruby /bin folder to user PATH if system PATH is not modifiable.


### 4. Install Sass
[http://sass-lang.com/install](http://sass-lang.com/install)

Sass is a CSS preprocessor that opens up fun things like variables, functions and selector nesting inside of our CSS.

1. Open Terminal or Command Prompt and run `sudo gem install sass`.
2. Type in the password to your computer's user account.
3. Ensure successful installation by then running `sass -v`.


### 5. Install Node
[http://nodejs.org](http://nodejs.org)

Node is a JavaScript application framework, and it's what we use for our local development servers.

1. Download and install NodeJS from the link above.
2. Open a command prompt and run `node -v` to ensure install was successful.
3. If on Windows, be sure to include "npm package manager" and to "Add to PATH".

### 6. Install Grunt
[http://gruntjs.com/getting-started](http://gruntjs.com/getting-started)

Grunt is a task runner for Node, and it makes the development process much easier.

1. Find instructions for installation at the link above (`sudo npm install -g grunt-cli`).
2. Open a command prompt and run `grunt -v` to ensure install was successful.
  * "Fatal error: Unable to find local grunt." still means the install was successful.

### 7. Install and Set Up SourceTree
[https://sourcetreeapp.com](https://sourcetreeapp.com)
Disclaimer: Use any git client you'd like, but all documentation will be for SourceTree.

1. Download and install Sourcetree.
2. Choose "Use an existing account" if you already have an Atlassian account, or choose "Go to My Atlassian" to create an account.  It's free, and you won't have to worry about it ever again after using it to log into Sourcetree.
3. Skip setup of Bitbucket or Github accounts.
4. (Windows Only) Ignore SSH key for now.
5. (Windows Only) Download an embedded version of git for SourceTree alone to use if not already installed.
6. (Windows Only) Choose that you do not want to use Mercurial.
7. Create an SSH key on your machine.
  1. Go to Tools > Create or Import SSH Keys.
  2. Click Generate, then move your mouse around to generate randomness.
  3. Copy the large generated text, and click Save private key (you are sure you want to save without a passphrase). Save this file to a location you will remember on your computer, and name it "id_rsa"
  4. Close the window and click Tools > Launch SSH Agent. This will appear to do nothing, but it starts a task in the system tray.
  5. Expand the system tray (arrow on the right side of the taskbar near the date and time), and double click Pageant.
  6. Click Add Key and add the id_rsa.ppk file you saved in step 3.
  7. Be sure to keep the SSH key you copied in step 3 in your clipboard.
8. Assign your SSH key to your Gitlab account.
  1. Log into Gitlab, and go to your profile settings ([https://gitlab.perficientxd.com/profile](https://gitlab.perficientxd.com/profile)).
  2. Go to the SSH Keys tab along the top of the page, and paste in the SSH key we generated and copied in step 3.
  3. This will automatically create a title, but you can change it to something more meaningful if you'd like (SSH Key for Ben's Work Computer).

With all these steps completed, you can now move onto cloning the git repository and reading up on the [Gitflow](gitflow.html).
