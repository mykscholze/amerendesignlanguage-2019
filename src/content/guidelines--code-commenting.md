## Code Commenting

For extensibility and code legibility, comments are required for all front-end code written in Project Atlas. Minification with Grunt tasks will keep the size of our production-ready files down by stripping the comments out, but our source code should be well documented.

### HTML

Each component should have a standard HTML comment immediately proceeding it with a short description of what the component is.  There will be no need to comment layout elements (e.g. Bootstrap grid elements), only the components within them.

```html
<!-- Primary header navigation -->
<nav class="primary-nav">
 ...
</nav>
```

### CSS / Sass

Sass allows us to use more than just the standard `/* */` style of CSS comments.  As such we can use hierarchical comments in our stylesheets and break them up into sections.  The main types of comments that should be adhered to are section comments, selector comments and property comments (as necessary).

```sass
//== Section title
//
//## Section description text.
```

```sass
//** Selector (or related set of selectors) description
selector {
  height: 100%; // property comment
}
```
