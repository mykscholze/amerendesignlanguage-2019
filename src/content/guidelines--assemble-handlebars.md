## Assemble (Handlebars)

Handlebars is a client-side templating framework, and there is quite a lot that we can do with it and Assemble.  There is [extensive documentation on all of this at Assemble's website](http://assemble.io/docs/index.html), but we'll go over the basics here.  The majority of what we'll use Handlebars for is the partial imports.  More on that in the coming paragraphs.

First, let's look at how our files are organized.

```
src/
│
├─── assets/
│   │
│   ├─── images/
│   ├─── scripts/
│   ├─── responses/
│   └─── styles/
│
├─── content/
│
├─── data/
│
└─── templates/
    │
    ├─── layouts/
    ├─── pages--site/
    ├─── pages--styleguide/
    └─── partials/
        │
        ├─── Components/
        ├─── Elements/
        ├─── Objects/
        └─── Utilities/
```

Any and all HTML code resides in side the `templates/` directory, and all files will have the Handlebars extension `.hbs`.

The `layouts/` folder contains our base page templates.  Any pages that follow the default "header, content section, footer" layout will inherit the default layout.  When a new layout requirement is discovered (e.g. two column, three column, etc), new layouts will be placed here.

All `.hbs` files inside the `pages/` folders are compiled and mapped directly to `.html` files in the `dist/` folder, so the `pages/` directories are a one-to-one mapping of each page on the site.  These files inherit a layout, and as such only the content is present in the page file (the header and footer are defined in the layout).  The power of Assemble and Handlebars is present on these pages in the form of YAML front-matter.  You can define page options at the top of the file with the following syntax:

```yaml
---
layout: new-layout.hbs
title: Page Title
---
```

If the layout property is not defined, the default layout will be inherited; simply defining the layout name will inherit other layouts.  Each page will need to specify a title, which is injected into the `<title>` tag.  Other options like data, JavaScript files, or other page variables can be defined here, but are not required.

The partials folder contains the real meat of our project.  This is our components directory.  They’ve been broken down into elements, objects, components, and utilities.  Each component should map out to a component that will be integrated into Sitecore, and any “sub-components” of the component can be broken down further into a molecule.  This becomes especially helpful when the content and data of a molecule is abstracted out, making our partials much more reusable.  Here is an example of putting the header partial / component / organism onto our page.

In the default.hbs layout, we import the header partial using the Handlebars import syntax.


It takes the file name as the first parameter, and searches all directories for a partial named `header.hbs`.  Subdirectory structure is not taken into consideration, as you only define “header” for the import, not “components/organisms/header”.  As such, each partial will need to have a unique name, regardless of directory structure.  BEM syntax helps with this, as you can define the organism name as the block, and the molecule name as the element.

Inside our `header.hbs` organism, we can define our search form, which has a molecule for the input and search button combination (this input with a button pattern can be used many other places, so it makes sense to create a molecule out of it that other organisms can consume.

```handlebars
## header.hbs

<!-- Search Form -->
<form id="header-search-form" method="post" action="#">
  {{!> form__input-inline-btn header-search }}
</form>
```
```handlebars
## form__input-inline-btn.hbs

<div class="input-inline-btn {{ component.classes }}">

	<!-- Search input field -->
	<input type="search"
            class="input-inline-btn__input {{ input.classes }}"
            placeholder="{{ input.placeholder }}">

	<!-- Search submit button -->
	<button type="submit"
             class="input-inline-btn__btn {{ btn.classes }}">
         <span class="sr-only">{{ btn.value }}</span>
    </button>
</div>
```
```json
## header-search.json

{
  "component": {
    "classes": "header-search"
  },
  "input": {
    "classes": "",
    "placeholder": "Search, e.g. \"code samples\""
  },
  "btn": {
    "classes": "fa fa-search",
    "value": ""
  }
}
```

The header partial is importing another partial called `form__input-inline-btn`.  The second parameter in that import is defining a data file (header-search.json) to be sent to that partial.  Each data file is placed into our `data/` directory, and can include whatever data you need a partial to have (class names, placeholder text, values, etc).  We have a form partial for an input field and submit button that has all of its data abstracted out of the actual file, and is pulling that information from a separate data file.  Building the form this way, we can reuse that `form__input-inline-btn` partial as many times as we want, and never have to touch the HTML.  The class name, placeholder text, and the icon of the button are all defined outside of the code.
