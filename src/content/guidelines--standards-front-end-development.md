# Standards and Guidelines for Front-End Development

## Table of Contents
1. [Coding Standards](#coding-standards)
  * [Naming Conventions](#naming-conventions)
  * [Code Commenting](#code-commenting)
  * [Atomic Design](#atomic-design)
  * [Assemble (Handlebars)](#assemble-handlebars-)
  * [Sass Usage](#sass-usage)
2. [Guidelines for Components](#guidelines-for-components)
  * [WAI-ARIA](#wai-aria)

---

## CODING STANDARDS

---

### Naming Conventions

We use [BEM syntax](https://en.bem.info/method/key-concepts/) in our HTML and CSS code.  BEM stands for Block, Element, Modifier, and it's not only a naming convention, but it changes the way you think about organizing your code.  The basics of the syntax are as follows:

```css
.block {}
.block__element {}
.block__element--modifer {}
```

Two underscores separate blocks from elements, and two dashes separate either blocks or elements from modifiers.

#### What is a block?

A block is your high-level component.  An example of a block is a search form.

```css
.search-form {}
```

#### What is an element?

Elements are the pieces that make up the block; the children of the component.  In our search form block, the elements would be the input field and submit buttons.

```css
.search-form__input {}
.search-form__submit-btn {}
```

#### What is a modifier?

A modifier is how you notate state changes or alternate versions of a block or element.  If our search form has a vertical layout by default, we can create a modifier class that changes it to a horizontal layout.

```css
.search-form--inline {}
```

#### Other naming convention considerations

* One dash should be used for multi-word blocks, elements or modifiers.
* camelCasing or under_scores should *not* be used to separate words.

#### Further Reading

* [MindBEMding: getting your head around BEM syntax](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
* [BEM 101](https://css-tricks.com/bem-101/)

---

### Code Commenting

For extensibility and code legibility, comments are required for all front-end code written in Project Atlas. Minification with Grunt tasks will keep the size of our production-ready files down by stripping the comments out, but our source code should be well documented.

#### HTML

Each component should have a standard HTML comment immediately proceeding it with a short description of what the component is.  There will be no need to comment layout elements (e.g. Bootstrap grid elements), only the components within them.

```html
<!-- Primary header navigation -->
<nav class="primary-nav">
 ...
</nav>
```

#### CSS / Sass

Sass allows us to use more than just the standard `/* */` style of CSS comments.  As such we can use hierarchical comments in our stylesheets and break them up into sections.  The main types of comments that should be adhered to are section comments, selector comments and property comments (as necessary).

```sass
//== Section title
//
//## Section description text.
```

```sass
//** Selector (or related set of selectors) description
selector {
  height: 100%; // property comment
}
```

---

### Atomic Design

[Atomic Design](http://bradfrost.com/blog/post/atomic-web-design/) is the principle we will be following for writing our HTML components – breaking them up into the smallest possible chunks for reusability and consistency.  Atomic Design and BEM syntax were made for each other, as the same rules apply to both.  

There are three key ingredients to Atomic Design: atoms, molecules and organisms.  Atoms are the basic building blocks of our development.  They are our individual HTML elements (links, buttons, input fields, paragraphs), or even abstractly speaking colors and font-sizes. When you start taking the atoms and putting them together, you get molecules. Molecules are more involved pieces of code than atoms, but most of the time they aren’t complex enough to be considered full components.  For example, a search form or a list of links might be considered molecules.  Putting together multiple molecules will now get us our organisms, or what we will call our full HTML / AEM components.  They are the complex collection of elements that make up our pages.  Taking a logo molecule, search form molecule and navigation molecule and putting them together, we get our header organism (component in AEM).

There are a lot of similarities between how you break up your code with Atomic Design and how you break up your code with BEM, which is why we’re using them in conjunction.  Though, the only standards to apply to Atomic Design are with your thought process.  This is a pattern that gets you in the proper mindset for thinking about how to componentize your HTML, and as such how you do it is at your discretion.  The general rule of thumb is to split up your code into as many reusable, flexible pieces as possible.  A link list can have many different flavors, but CSS classes should control those different styles.  The link list HTML should be the same on any page, no matter what the design calls for.

We won't be sticking strictly to the principles of Atomic Design for this project, but we're basing our elements and components off this model.  Make the smallest component you can, bigger components inherit from smaller ones.


---

### Assemble (Handlebars)

Handlebars is a client-side templating framework, and there is quite a lot that we can do with it and Assemble.  There is [extensive documentation on all of this at Assemble's website](http://assemble.io/docs/index.html), but we'll go over the basics here.  The majority of what we'll use Handlebars for is the partial imports.  More on that in the coming paragraphs.

First, let's look at how our files are organized.

```
src/
│
├─── assets/
│   │
│   ├─── images/
│   ├─── scripts/
│   ├─── responses/
│   └─── styles/
│
├─── content/
│
├─── data/
│
└─── templates/
    │
    ├─── layouts/
    ├─── pages--site/
    ├─── pages--styleguide/
    └─── partials/
        │
        ├─── Components/
        ├─── Elements/
        ├─── Objects/
        └─── Utilities/
```

Any and all HTML code resides in side the `templates/` directory, and all files will have the Handlebars extension `.hbs`.

The `layouts/` folder contains our base page templates.  Any pages that follow the default "header, content section, footer" layout will inherit the default layout.  When a new layout requirement is discovered (e.g. two column, three column, etc), new layouts will be placed here.

All `.hbs` files inside the `pages/` folders are compiled and mapped directly to `.html` files in the `dist/` folder, so the `pages/` directories are a one-to-one mapping of each page on the site.  These files inherit a layout, and as such only the content is present in the page file (the header and footer are defined in the layout).  The power of Assemble and Handlebars is present on these pages in the form of YAML front-matter.  You can define page options at the top of the file with the following syntax:

```yaml
---
layout: new-layout.hbs
title: Page Title
---
```

If the layout property is not defined, the default layout will be inherited; simply defining the layout name will inherit other layouts.  Each page will need to specify a title, which is injected into the `<title>` tag.  Other options like data, JavaScript files, or other page variables can be defined here, but are not required.

The partials folder contains the real meat of our project.  This is our components directory.  They’ve been broken down into elements, objects, components, and utilities.  Each component should map out to a component that will be integrated into Sitecore, and any “sub-components” of the component can be broken down further into a molecule.  This becomes especially helpful when the content and data of a molecule is abstracted out, making our partials much more reusable.  Here is an example of putting the header partial / component / organism onto our page.

In the default.hbs layout, we import the header partial using the Handlebars import syntax.


It takes the file name as the first parameter, and searches all directories for a partial named `header.hbs`.  Subdirectory structure is not taken into consideration, as you only define “header” for the import, not “components/organisms/header”.  As such, each partial will need to have a unique name, regardless of directory structure.  BEM syntax helps with this, as you can define the organism name as the block, and the molecule name as the element.

Inside our `header.hbs` organism, we can define our search form, which has a molecule for the input and search button combination (this input with a button pattern can be used many other places, so it makes sense to create a molecule out of it that other organisms can consume.

```handlebars
## header.hbs

<!-- Search Form -->
<form id="header-search-form" method="post" action="#">
  {{!> form__input-inline-btn header-search }}
</form>
```
```handlebars
## form__input-inline-btn.hbs

<div class="input-inline-btn {{ component.classes }}">

	<!-- Search input field -->
	<input type="search"
            class="input-inline-btn__input {{ input.classes }}"
            placeholder="{{ input.placeholder }}">

	<!-- Search submit button -->
	<button type="submit"
             class="input-inline-btn__btn {{ btn.classes }}">
         <span class="sr-only">{{ btn.value }}</span>
    </button>
</div>
```
```json
## header-search.json

{
  "component": {
    "classes": "header-search"
  },
  "input": {
    "classes": "",
    "placeholder": "Search, e.g. \"code samples\""
  },
  "btn": {
    "classes": "fa fa-search",
    "value": ""
  }
}
```

The header partial is importing another partial called `form__input-inline-btn`.  The second parameter in that import is defining a data file (header-search.json) to be sent to that partial.  Each data file is placed into our `data/` directory, and can include whatever data you need a partial to have (class names, placeholder text, values, etc).  We have a form partial for an input field and submit button that has all of its data abstracted out of the actual file, and is pulling that information from a separate data file.  Building the form this way, we can reuse that `form__input-inline-btn` partial as many times as we want, and never have to touch the HTML.  The class name, placeholder text, and the icon of the button are all defined outside of the code.

---

### Sass Usage

Any CSS written will be put into `.scss` files inside the `/src/assets/sass/` directory.  All of these stylesheets get compiled together into one `main.css` file when Grunt does a build.  Each new stylesheet created should be prefixed with an underscore to assign them as partials and open them up to our variables and mixins.  In order to see any new styles, after the stylesheet has been created it should be added to the import list in `main.scss`.

Following our principles of Atomic Design, stylesheets should be broken up into atoms, molecules and organisms.

* Atom stylesheets are for general styles on individual elements, or for helper classes.
* Molecule stylesheets are for the very basic structural styling of reusable molecules.  These should be limited to what makes that molecule that molecule (e.g. search form has 30px height and 100% width, search button absolutely positioned to the right).  Alternative styling should be abstracted out from the base styles.
* Organism stylesheets are for component-specific styles.  Any style you would apply to the organism OR styles for the molecules inside that only apply to that specific organism.  For example, the search form contained in the header might have a margin-top of 2rem, but that's not to say every search form placed on the site should have a margin-top of 2rem.  That margin-top rule should be kept localized to the header organism stylesheet.

---

## GUIDELINES FOR COMPONENTS

---

This component guidelines section will be continuously updated as more component solutions are identified.  Always refer back to this document when looking for documentation on a specific component.

---
### WAI-ARIA

WAI-ARIA (Web Accessibility Initiative - Accessible Rich Internet Applications), also known as ARIA, is a specification that describes how to increase the accessibility of a page.  Below are links to the specification on best practices for ARIA development.

This [article on using ARIA in HTML](http://w3c.github.io/aria-in-html/) is required reading.  It's one thing to know what ARIA roles, states, and properties to use, but it's more important to know when and when not to use them.

* [Alphabetical list of all ARIA roles](http://www.w3.org/TR/wai-aria/roles#role_definitions_header)
* [Alphabetical list of all ARIA states and properties](http://www.w3.org/TR/wai-aria/states_and_properties#state_prop_def_header)
* [Wave Toolbar for accessibility testing](https://wave.webaim.org/toolbar/) (Chrome extension also available)

---


end
