## Naming Conventions

We use [BEM syntax](https://en.bem.info/method/key-concepts/) in our HTML and CSS code.  BEM stands for Block, Element, Modifier, and it's not only a naming convention, but it changes the way you think about organizing your code.  The basics of the syntax are as follows:

```css
.block {}
.block__element {}
.block__element--modifer {}
```

Two underscores separate blocks from elements, and two dashes separate either blocks or elements from modifiers.

### What is a block?

A block is your high-level component.  An example of a block is a search form.

```css
.search-form {}
```

### What is an element?

Elements are the pieces that make up the block; the children of the component.  In our search form block, the elements would be the input field and submit buttons.

```css
.search-form__input {}
.search-form__submit-btn {}
```

### What is a modifier?

A modifier is how you notate state changes or alternate versions of a block or element.  If our search form has a vertical layout by default, we can create a modifier class that changes it to a horizontal layout.

```css
.search-form--inline {}
```

### Other naming convention considerations

* One dash should be used for multi-word blocks, elements or modifiers.
* camelCasing or under_scores should *not* be used to separate words.

### Further Reading

* [MindBEMding: getting your head around BEM syntax](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
* [BEM 101](https://css-tricks.com/bem-101/)
