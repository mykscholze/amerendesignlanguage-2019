## WAI-ARIA

WAI-ARIA (Web Accessibility Initiative - Accessible Rich Internet Applications), also known as ARIA, is a specification that describes how to increase the accessibility of a page.  Below are links to the specification on best practices for ARIA development.

This [article on using ARIA in HTML](http://w3c.github.io/aria-in-html/) is required reading.  It's one thing to know what ARIA roles, states, and properties to use, but it's more important to know when and when not to use them.

* [Alphabetical list of all ARIA roles](http://www.w3.org/TR/wai-aria/roles#role_definitions_header)
* [Alphabetical list of all ARIA states and properties](http://www.w3.org/TR/wai-aria/states_and_properties#state_prop_def_header)
* [Wave Toolbar for accessibility testing](https://wave.webaim.org/toolbar/) (Chrome extension also available)
