require('Routes');
require('mock-responses');
require('Accordion');
require('Header');
// require('Footer');
// require('bootstrap-datepicker.min');
require('form-validation');
require('tooltips');
require('popovers');
require('password-toggle');
require('bootstrap-spinner');
require('code');
require('c-promo-carousels');
require('Utilities');
require('Tabs');

// $('.input-group.date').datepicker({
//   showOnFocus: false,
//   container: '.input-group',
//   orientation: 'bottom'
// });

$(function () {
  $('.input-group.date').datetimepicker({ format: 'L' });
  $('.input-group.time').datetimepicker({ format: 'LT' });
});