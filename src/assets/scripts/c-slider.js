var Slider = (function() {

  var init = function() {
    $('.c-slider--three-col').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        accessibility: false,
        responsive: [
          {
              breakpoint: 900,
              settings: {
                  slidesToShow: 2,
                  accessibility: true
              }
          },
          {
              breakpoint: 600,
              settings: {
                  slidesToShow: 1,
                  accessibility: true
              }
          }
        ]
    });
    $('.c-slider--one-col:not(".bkg")').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.bkg.c-slider--one-col'
    });

    $('.c-slider--one-col.bkg').slick({
        dots: false,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });
  }

  return {
      init: init
  };
})();

$(document).ready(function(){
  Slider.init();
});
