var Hero = (function() {

  var sizeImage = function() {
    var $img = $('.c-hero > img');
    var ratio = $img.width() / $img.height();
    var minHeight = 495;

    $img.css({
      'min-height': minHeight + 'px',
      'min-width': minHeight * ratio + 'px'
    })
  }

  var init = function() {

    sizeImage();

    $('.c-hero__login-form').submit(function(e){
      var $this = $(this);

      if ($this.find('fieldset').is(":hidden")) {
        e.preventDefault();
        $this.find('fieldset').slideDown(300);
      }
    });
  }

  return {
      init: init
  };
})();

$(document).ready(function(){
  Hero.init();
});
