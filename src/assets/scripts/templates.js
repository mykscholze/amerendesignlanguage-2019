this["Ameren"] = this["Ameren"] || {};
this["Ameren"]["Templates"] = this["Ameren"]["Templates"] || {};

this["Ameren"]["Templates"]["start-service__step-4"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col-sm-6 col-sm-offset-3\">\n      <h2>Tell us your start date</h2>\n    </div>\n  </div>\n</div>\n";
},"useData":true});