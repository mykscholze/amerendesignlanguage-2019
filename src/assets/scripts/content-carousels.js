var ContentCarousels = (function() {

    var optionsDefault = {
        dots: true,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        accessibility: false,
        prevArrow: '<button type="button" class="slick-prev" aria-label="Previous"><svg viewBox="0 0 24 24"><polyline points="10.5 4.5 17.66 11.99 10.5 19.5"/></svg></button>',
        nextArrow: '<button type="button" class="slick-next" aria-label="Next"><svg viewBox="0 0 24 24"><polyline points="10.5 4.5 17.66 11.99 10.5 19.5"/></svg></button>'
    };

    var optionsTwoCol = {
        slidesToShow: 2,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    }

    var optionsThreeCol = {
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    }
    
    var init = function() {

        var $contentCarousels = $('.content-carousel, .media-carousel');

        $contentCarousels.each(function(i, carousel) {
            var cols = $(carousel).data('cols');
            
            switch (cols) {
                case 3:
                    $(carousel).slick($.extend({}, optionsDefault, optionsThreeCol));
                    break;
                case 2:
                    $(carousel).slick($.extend({}, optionsDefault, optionsTwoCol));
                    break;
                default:
                    $(carousel).slick(optionsDefault);
            }
        });

        $('.content-carousel').on('setPosition', function () {
            $(this).find('.slick-slide').height('auto');
            var slickTrack = $(this).find('.slick-track');
            var slickTrackHeight = $(slickTrack).height();
            $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
        });
    }
  
    return {
        init: init
    };
  })();
  
  $(document).ready(function(){
    ContentCarousels.init();
  });
  