$(document).ready(function() {
  $('[data-toggle="password"]').on("mousedown", function(e) {

    var $toggle  = $(this),
        showText = $toggle.data('show'),
        hideText = $toggle.data('hide'),
        $input   = $toggle.siblings('input'),
        value    = $input.val();

    if ($toggle.text() == showText) {
      $toggle.text(hideText);
      $input.prop('type', 'text');
    } else {
      $toggle.text(showText);
      $input.prop('type', 'password');
    }

    setTimeout(function(){ $input.focus().val('').val(value); }, 200);
  });
});
