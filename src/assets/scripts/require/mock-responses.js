// order matters: for the same url, put more specific request matchers first
var mockResponses = [

{
	url: Routes.getAddress,
	type: "GET",
	data: {
		state: "MO"
	},
	proxy: 'assets/json/success-response.json'
},

{
	url: Routes.getAddress,
	status: 404
}];

var defaults = {
	responseTime: 200,
	status: 200
};

function initMockResponse(i, mockConfig) {
	var config = $.extend({}, defaults, mockConfig);

	$.mockjax(config);
}

$.each(mockResponses, initMockResponse);
