var Header = (function() {

  var isShowing = false;
  
  // This event fires immediately when the show instance method is called.
  var onNavbarShow = function(){
    isShowing = true;
    $('html, main').addClass('menu-open');
    $('.navbar-collapse').not(this).collapse('hide');
  }

  // This event is fired when a collapse element has been made visible 
  // to the user (will wait for CSS transitions to complete).
  var onNavbarShown = function(){
    isShowing = false;
    // $('html, main').addClass('menu-open');
  }

  // This event is fired immediately when the hide method has been called.
  var onNavbarHide = function(){
    if (!isShowing) {
      $('html, main').removeClass('menu-open');
    }
  }

  var init = function() {
    $('.navbar-collapse').on('show.bs.collapse', onNavbarShow);
    $('.navbar-collapse').on('shown.bs.collapse', onNavbarShown);
    $('.navbar-collapse').on('hide.bs.collapse', onNavbarHide);
  }

  return {
      init: init
  };
})();

$(document).ready(function(){
  Header.init();
});
