/* Configure URLs for Ajax Requests, or Page URLs to JSON API */
var Routes = {
	getAddress: '/get-address'
};

/* expose as global variable */
window.Routes = Routes;
