var Tabs = (function() {

    var getInitialItemsWidth = function(el) {
        var totalItemsWidth = 0;

        $.each($(el).children('li'), function(i, li){
            totalItemsWidth += $(li).width();
        });
        
        $(el).data('total-items-width', Math.ceil(totalItemsWidth) + 2);
    }

    var checkTabs = function(el) {
        var containerWidth = $(el).width();

        if ($(el).data('total-items-width') < containerWidth && !$(el).hasClass('nav-tabs--justified')) {
            $(el).addClass('nav-tabs--justified');
        } else if ($(el).data('total-items-width') >= containerWidth && $(el).hasClass('nav-tabs--justified')) {
            $(el).removeClass('nav-tabs--justified');
        }
    }
  
    var init = function() {
        var $tabs = $('.nav-tabs');
  
        $tabs.each(function(i){
            getInitialItemsWidth(this);
            checkTabs(this);
        });

        $(window).resize(function(){
            $tabs.each(function(i){
                checkTabs(this);
            }); 
        });

        $tabs.on('shown.bs.tab', function (e) { $(e.target).blur() });
    }
  
    return {
        init: init
    };
  })();

$(document).ready(function(){
    Tabs.init();
});
  