var PromoCarousels = (function() {

    var optionsDefault = {
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        accessibility: false,
        prevArrow: '<button type="button" class="slick-prev" aria-label="Previous"><svg viewBox="0 0 24 24"><polyline points="10.5 4.5 17.66 11.99 10.5 19.5"/></svg></button>',
        nextArrow: '<button type="button" class="slick-next" aria-label="Next"><svg viewBox="0 0 24 24"><polyline points="10.5 4.5 17.66 11.99 10.5 19.5"/></svg></button>'
    };
    
    var init = function() {

        var $promoCarousels = $('.c-promo-carousel');

        $promoCarousels.each(function(i, carousel) {
            $(carousel).slick(optionsDefault);
            $(carousel).on('setPosition', function () {
                $(this).find('.slick-slide').height('auto');
                var slickTrack = $(this).find('.slick-track');
                var slickTrackHeight = $(slickTrack).height();
                $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
            });
        });

    }
  
    return {
        init: init
    };
  })();
  
  $(document).ready(function(){
    PromoCarousels.init();
  });
  