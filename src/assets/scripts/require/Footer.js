var Footer = (function() {

  var bumpIt = function() {
    $('body').css('padding-bottom', $('.footer').height());
  }

  var init = function() {
    bumpIt();

    $(window).resize(function() {
      bumpIt();
    });
  }

  return {
      init: init
  };
})();

$(document).ready(function(){
  Footer.init();
});
