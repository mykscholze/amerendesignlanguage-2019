// Enable form submission if invalid by default (will still validate)
$.fn.validator.Constructor.DEFAULTS.disable = false;

// Example of custom validator
// usage: <input type="text" data-equals="foo">
$.fn.validator.Constructor.DEFAULTS.custom.equals = function($el) {
  var matchValue = $el.data("equals") // foo
  if ($el.val() !== matchValue) {
    return "Hey, that's not valid! It's gotta be " + matchValue
  }
}

// Initiate form validation for all forms
