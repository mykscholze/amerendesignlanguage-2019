﻿var gulp  = require('gulp'),
     path       = require('path');
     jshint     = require('gulp-jshint');
     sass       = require('gulp-sass');
     concat     = require('gulp-concat');
     uglify     = require('gulp-uglify');
     rename     = require('gulp-rename');
     minifyCSS  = require('gulp-clean-css');
     browserify = require('browserify');
     buffer     = require('vinyl-buffer');
     babelify   = require('babelify');
     wrap       = require('gulp-wrap');
     source     = require('vinyl-source-stream');
     qunit      = require('gulp-qunit');
var hbs = require('gulp-hbs');


// Lint Task
gulp.task('lint', function() {
  gulp.src('dev/sweetalert.es6.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));

  return gulp.src('dev/*/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {

  gulp.src('./src/assets/styles/main.scss')
    .pipe(sass())
    .pipe(concat('main.css'))
    .pipe(gulp.dest('dist'));

});




// Concatenate & Minify JS
gulp.task('scripts', function() {
  return browserify({
      entries: './bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
      debug: true
    })
    .transform(babelify)
    .bundle()
    .pipe(source('bootstrap.min.js'))
    .pipe(wrap({
      src: './bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js'
    }))
    .pipe(gulp.dest('dist')) // Developer version

    .pipe(rename('bootstrap.min.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('dist')); // User version
});

gulp.task('test', function() {
  return gulp.src('./test/index.html')
    .pipe(qunit({
      timeout: 20
    }));
});

  gulp.src('*.json')
        .pipe(hbs('./src/templates/pages--site/index.hbs'))
        .pipe(gulp.dest('html'));
// Watch Files For Changes
gulp.task('watch', function() {
  gulp.watch(['dev/*.js', 'dev/*/*.js'], ['lint', 'scripts']);
  gulp.watch(['dev/*.scss', 'dev/*.css'], ['sass']);
});

// Default Task
gulp.task('default',
  gulp.series('lint', function(done) {
    done();
  }),
  gulp.series('sass', function(done) {
    done();
  }),
  gulp.series('scripts', function(done) {
    done();
  }),
  gulp.series('watch', function(done) {
    done();
  }),
  gulp.series('test', function(done) {
    done();
  }));
